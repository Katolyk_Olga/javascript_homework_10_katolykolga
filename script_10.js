// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. 
// Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. 
// При цьому решта тексту повинна бути прихована. 
// У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, 
// і що вкладки можуть додаватися та видалятися. 
// При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

let correctView = document.querySelector(".centered-content");
correctView.style.justifyContent = 'initial'; 
// correctView.style.removeProperty('justify-content'); 
// прибрала justify-content: center, бо дивно веде себе сайт при зумі, перший список упливає за верхній край при збільшені.


// додаю клас з відповідними індексами в другий список з текстами та приховую видимість для всіх li:
let ulTabsContent = document.querySelector(".tabs-content");
    console.log(ulTabsContent);
    console.log(ulTabsContent.children);

let ulTabsContentObject = Array.from(ulTabsContent.children);
    console.log(ulTabsContentObject);

for (let index = 0; index < ulTabsContentObject.length; index++) {       
    ulTabsContent.children[index].setAttribute("data-tabs-content-id", index);
    ulTabsContent.children[index].hidden = true;  
    // ulTabsContent.children[index].display = "none"; 
    ulTabsContent.children[0].hidden = false;        
}


 // додаю клас з відповідними індексами в перший список з текстами,
 // а також в циклі додаю кожній li властивість з функцією зміни класа active:
let ulTabs = document.querySelector(".tabs");
    console.log(ulTabs);
    console.log(ulTabs.children);

let ulTabsObject = Array.from(ulTabs.children);
    console.log(ulTabsObject);

for (let index = 0; index < ulTabsObject.length; index++) {       
    ulTabs.children[index].setAttribute("data-tabs-title-id", index);  
    ulTabs.children[index].addEventListener('click', changeClassActive);       
}

// створюю функцію, яка прибирає всім і додає по кліку одному клас active     
function changeClassActive (event){
    ulTabsObject.forEach(item => item.classList.remove("active"));
    event.target.classList.add("active"); 
    console.log(event.target);  

    let generalTargetIndex = event.target.dataset.tabsTitleId;
    console.log(generalTargetIndex); // ми отримали індекс li з першого списку, на яку клікнули

    console.log(ulTabsContentObject[generalTargetIndex]);
    ulTabsContentObject.forEach(item => item.hidden = true);
    ulTabsContentObject[generalTargetIndex].hidden = false;             
}


// Варіант від ментора для роздумів:
// let ul = document.querySelector('.tabs'); 
// ul.addEventListener('click', function (ev) {
//     let data = ev.target.dataset.type; 
//     console.log(ev.target.dataset.type);     
//     console.log(document.querySelector('.active-p')); 
//     document.querySelector('.active-p').classList.remove('active-p');
//     console.log(document.querySelector('.active-tab')); 
//     document.querySelector('.active-tab').classList.remove('active-tab'); 
//     console.log(document.querySelector(`[data-li = ${data}]`)); 
//     document.querySelector(`[data-li = ${data}]`).classList.add('active-p'); 
//     ev.target.classList.add('active-tab') 
// });
